# FDSMeshLibDemo

### 工程介绍
FDSMeshLibDemo是FDSMeshLib iOS版集成例码
+ iOS库：FDSMeshLib.framework（动态库）
+ 支持的iOS版本：iOS9-最新
+ 开发语言：Objective-C
+ 编译环境：Xcode13及以上

### SDK路径
FDSMeshLibDemo -> ThirdPart -> FDSMeshLib -> FDSMeshLib.framework

### SDK接口文档
http://liuxuweimeng.gitee.io/fds-mesh-sdk/#/

### 代码仓库
https://gitee.com/liuxuweimeng/fdsmesh-lib-demo.git
https://www.jianshu.com/p/83be8c9a1396/

### 权限配置
+ UIFileSharingEnabled  文件共享权限
+ NSBluetoothPeripheralUsageDescription  蓝牙权限
+ NSBluetoothAlwaysUsageDescription 蓝牙权限
+ NSAppTransportSecurity 网络权限
+ UIBackgroundModes 蓝牙后台权限

### SDK集成配置
+ targets -> search paths -> framework search paths -> sdk相对路径
+ targets -> build phases -> link binary with libraries 需要添加sdk
+ targets -> build phases -> 新增new copy files phase -> destination 选择frameworks -> 添加sdk


