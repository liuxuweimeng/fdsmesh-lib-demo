# 小程序JS Mesh SDK说明文档

## 一.SDK说明

+ SDK文件：fds-mesh-util-v1_x_x.js
+ SDK路径：fds-mesh-demo/src/utils/fds-mesh-util-v1_x_x.js
+ 开发语言：javascript
+ 依赖插件：后台->设置->第三方设置->插件管理->新增"蓝牙Mesh"插件-wx013447465d3aa024
+ 小程序链接：#小程序://蓝牙Mesh工具/ZoeUex90OnKpRSq
+ 演示视频：https://v.youku.com/v_show/id_XNjM4MjkwMjg0OA==.html
+ 集成环境：小程序开发者工具/IDEA
+ node: https://nodejs.org/en/
  vue/mpvue: npm i -g @vue/cli-init
+ 安装依赖: yarn 或 npm install
+ 开发时构建: npm run dev
+ 打包构建: npm run build
+ DEMO技术栈：mpvue。DEMO编译后输出dist文件夹，由小程序开发者工具导入并配置appId即可运行。

## 二.DEMO工程介绍

### 1.下载和集成

+ 代码仓库（需要权限）：https://gitee.com/liuxuweimeng/fds-mesh-demo

### 2.权限配置

+ 小程序：scope.bluetooth

## 三.FdsMeshUtil接口

### 调用示例

```
Vue全局挂载-main.js
1.在main.js中引入 import FdsMeshUtil from "./utils/fds-mesh-util-v1_x_x";
2.const fdsMeshUtil = FdsMeshUtil();
3.Vue.prototype.fdsMeshUtil = fdsMeshUtil;
4.任何页面 this.fdsMeshUtil.xxxxx() 调用，例如 this.fdsMeshUtil.initMesh()
```

```
小程序全局挂载-app.js
1.将fds-mesh-util-v1_x_x.js文件中export default function FdsMeshUtil() 改为 function FdsMeshUtil()
2.导出module.exports = FdsMeshUtil();
3.在app.js中引入 fdsMeshUtil: require('./utils/fds-mesh-util-v1_x_x.js') 并将以下代码引入
onLaunch() {
    wx.setKeepScreenOn({
      keepScreenOn: true
    })
    // 注册全局方法
    this.enhancePage();
},
enhancePage() {
    // 挂载全局方法，this调用
    const oPage = Page;
    Page = config => oPage(Object.assign(config, {
      fdsMeshUtil,
    }));
},
4.任何页面 this.fdsMeshUtil.xxxxx() 调用，例如 this.fdsMeshUtil.initMesh()
```

### 1.初始化Mesh SDK

```
/**
 * 初始化Mesh SDK
 *
 * @desc 调用该接口前需要配置和申请好蓝牙权限
 * @return {Promise} 如果成功则回调SDK版本号
 */
function initMesh()
```

### 2.是否开启日志

```
/**
 * 是否开启日志
 *
 * @param {boolean} enabled - 是/否
 * @desc 日志可在后台查看，支持微信号过滤等
 */
function enableLog(enabled)
```

### 3.加载和切换Mesh网络

```
/**
 * 加载和切换Mesh网络
 *
 * @param {string} networkName - 网络名称
 * @desc 需要注意传参networkName需要唯一
 * @return {Promise} 如果成功则回调网络ID
 */
function changeNetwork(networkName)
```

### 4.加载和切换Mesh网络-扩展

```
/**
 * 加载和切换Mesh网络-扩展
 *
 * @param {string} networkName - 网络名称
 * @param {string} networkData - 网络数据
 * @param {appKey} AppKey - 方便固定AppKey使用
 * @desc 需要注意传参networkName需要唯一，networkData为base64字符串，
         该接口方便由服务器的存储数据导入到本地并加载
 * @return {Promise} 如果成功则回调网络ID
 */
function changeNetworkEx(networkName, networkData, appKey)
```

### 5.保存MESH JSON数据

```
/**
 * 保存MESH JSON数据
 */
function saveMeshData()
```

### 6.获取MESH JSON数据

```
/**
 * 获取MESH JSON数据
 * @return {string} MESH Base64数据
 */
function getMeshData()
```

### 7.获取Mesh网络名称

```
/**
 * 获取MESH网络名称
 * @return {string} MESH网络名称
 */
function getNetworkName()
```

### 8.获取Mesh网络ID

```
/**
 * 获取Mesh网络ID
 *
 * @return {string} 网络ID
 */
function getNetworkID()
```

### 9.获取Mesh网络秘钥

```
/**
 * 获取Mesh网络秘钥
 *
 * @return {string} 网络秘钥
 */
function getNetworkKey()
```

### 10.获取App秘钥

```
/**
 * 获取App秘钥
 *
 * @return {string} App秘钥
 */
function getApplicationKey()
```

### 11.开始扫描Mesh设备

```
/**
 * 开始扫描Mesh设备
 *
 * @param {number} timeout - 超时返回时间
 * @param {boolean} isReset 当前参数可不传默认是true, 表示扫描过程中会默认清除脏节点
 * @desc 设备列表数据按信号强度由高到低顺序返回
 * @return {Promise} 如果成功则回调未入网的设备列表
 */
function startScanMeshDevice(timeout, isReset = true)
```

### 12.停止扫描Mesh设备

```
/**
 * 停止扫描Mesh设备
 */
function stopScanMeshDevice()
```

### 13.根据设备ID获取扫描到的设备对象

```
/**
 * 根据设备ID获取扫描到的设备对象
 *
 * @param {string} deviceId - 设备ID
 * @desc 注意这边一般指扫描到的未入网的设备
 * @return {object} 设备对象
 */
function getScanDevice(deviceId)
```

### 14.根据地址获取已入网的节点对象

```
/**
 * 根据地址获取已入网的节点对象
 *
 * @param {number} address - 节点地址
 * @return {object} 节点对象
 */
function getNodeByAddress(address)
```

### 15.根据设备ID获取已入网的节点单播地址

```
/**
 * 根据设备ID获取已入网的节点单播地址
 *
 * @param {string} deviceId - 节点ID
 * @return {number} 单播地址
 */
function getUnicastAddress(deviceId)
```

### 16.获取节点列表

```
/**
 * 获取节点列表
 *
 * @return {array} 节点列表
 */
function getNodes()
```

### 17.获取未订阅组的节点列表

```
/**
 * 获取未订阅组的节点列表
 *
 * @return {array} 节点列表
 */
function getNodesWithoutGroup()
```

### 18.创建组

```
/**
 * 创建组
 *
 * @param {string} name - 组名称
 * @param {number} address - 组地址
 * @desc 注意address需要调用getLocalGroupAddress接口获取
 */
function createGroup(name, address)
```

### 19.删除组

```
/**
 * 删除组
 *
 * @param {number} address - 组地址
 * @desc 注意当前组里配置节点的情况下，调用该接口无效
 */
function removeGroup(address)
```

### 20.获取组列表

```
/**
 * 获取组列表
 *
 * @return {array} 组列表
 */
function getGroups()
```

### 21.根据地址获取组对象

```
/**
 * 根据地址获取组对象
 *
 * @param {number} address - 组地址
 * @return {object} 组对象
 */
function getGroupByAddress(address)
```

### 22.连接代理设备

```
/**
 * 连接代理设备
 *
 * @param {string} deviceId - 设备ID
 */
function proxyConnect(deviceId)
```

### 23.自动连接代理设备

```
/**
 * 自动连接代理设备
 *
 * @param {string} deviceId - 设备ID
 * @param {function} onConnected - 回调方法，成功返回true，失败返回false
 * @desc 会优先连接指定传入的设备，根据实际情况自动连接代理
 */
function proxyAutoConnect(deviceId, onConnected)
```

### 24.自动连接代理设备-队列任务

```
/**
 * 自动连接代理设备
 *
 * @param {string} deviceId - 设备ID
 * @param {function} onConnected - 回调方法，成功返回true，失败返回false
 * @desc 会优先连接指定传入的设备，根据实际情况自动连接代理
 */
function proxyAutoQueueConnect(deviceId, onConnected)
```

### 25.断开代理设备

```
/**
 * 断开代理设备
 */
function proxyDisconnect()
```

### 26.获取代理设备ID

```
/**
 * 获取代理设备ID
 *
 * @return {string} 设备ID
 */
function getConnectedId()
```

### 27.判断代理设备是否连接

```
/**
 * 判断代理设备是否连接
 *
 * @return {boolean} 是/否连接
 */
function isConnected()
```

### 28.设备配网

```
/**
 * 设备配网
 *
 * @param {object} device - 未入网的设备
 * @param {string} networkId - 网络ID
 * @return {Promise} 如果成功/失败则回调结果
 */
function joinNetwork(device, networkId)
```

### 29.批量设备配网

```
/**
 * 批量设备配网
 *
 * @param {array} devices - 未入网的设备列表
 * @param {function} onResult - 回调成功返回true，否则为false
 */
function batchJoinNetwork(devices, onResult)
```

### 30.设备退网

```
/**
 * 设备退网
 *
 * @param {number} address - 节点单播地址
 * @return {Promise} 如果成功/失败则回调结果
 */
function resetDevice(address)
```

### 31.绑定节点和Model的秘钥

```
/**
 * 绑定节点和Model的秘钥
 *
 * @param {number} address - 节点单播地址
 * @desc 不执行该步骤，会无法发送自定义数据
 * @return {Promise} 如果成功/失败则回调结果
 */
function bindKeyModel(address)

```

### 32.强制删除脏节点

```
/**
 * 强制删除脏节点
 *
 * @param {number} address - 节点单播地址
 */
function forceResetDevice(address)
```

### 33.获取本地新增的组地址

```
/**
 * 获取本地新增的组地址
 *
 * @desc 主要用于新增的组使用，如果服务器可以分配则由服务器分配
 * @return {number} 组地址
 */
function getLocalGroupAddress()
```

### 34.配置订阅组

```
/**
 * 配置订阅组
 *
 * @param {number} nodeAddress - 节点地址
 * @param {number} groupAddress - 组地址
 * @return {Promise} 如果成功/失败则回调结果
 */
function configSubscribe(nodeAddress, groupAddress)
```

### 35.配置取消订阅组

```
/**
 * 配置取消订阅组
 *
 * @param {number} nodeAddress - 节点地址
 * @param {number} groupAddress - 组地址
 * @return {Promise} 如果成功/失败则回调结果
 */
function configUnSubscribe(nodeAddress, groupAddress)
```

### 36.发送自定义数据

```
/**
 * 发送自定义数据
 *
 * @param {number} address - 节点地址
 * @param {number} opcode - 操作码
 * @param {arrayBuffer} data - 自定义数据
 * @desc 该接口不含绑秘钥流程，需要配合bindKeyModel接口使用
 * @return {Promise} 如果成功/失败则回调结果
 */
function sendCustomData(address, opcode, data)
```

### 37.发送消息数据

```
/**
 * 发送消息数据
 *
 * @param {number} address - 节点地址
 * @param {number} opcode - 操作码
 * @param {arrayBuffer} data - 自定义数据
 * @desc 该接口包含绑秘钥流程，组播和全播情况下因设备掉电情况可能会卡住
 * @return {Promise} 如果成功/失败则回调结果
 */
function sendMessageData(address, opcode, data)
```

### 38.设置回响应监听

```
/**
 * 设置回响应监听
 *
 * @param {function} onMessage - 回响应方法
 * @desc 回响应里包含source, opcode, parameters参数
 */
function onMessageListener(onMessage)
```

### 39.取消设置回响应监听

```
/**
 * 取消设置回响应监听
 *
 * @param {function} onMessage - 回响应方法
 * @desc 回响应里包含source, opcode, parameters参数
 */
function offMessageListener(onMessage)
```

### 40.设置网络数据更新的监听

```
/**
 * 设置网络数据更新的监听
 *
 * @param {function} onUpdate - 回响应方法
 * @desc 该方法主要方便服务器实时更新网络数据
 */
function onNetworkUpdateListener(onUpdate)
```

### 41.转换分享数据

```
/**
 * 转换分享数据
 *
 * @param {string} data - 网络数据
 * @param {map} mapData - {mac:uuid}映射
 * @param {boolean} isImport - true表示导入的数据，false表示导出的数据
 * @desc 该方法主要用语MESH网络多设备，跨平台共享
 * @return 转换后的网络数据
 */
function transformNetworkData(data, mapData, isImport)
```
