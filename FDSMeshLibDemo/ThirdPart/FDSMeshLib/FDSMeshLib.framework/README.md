# FDSMeshLib

### 远程仓库
    https://gitee.com/liuxuweimeng/fdsmesh-lib.git
    cd ...
    git init
    git add .
    git commit -m"1.0.0"
    git remote add origin https://github.com/xxxxx/xxxxxx.git


### V1.6.3
    1.优化日志最大存储大小为20M，每10*60秒检查一次日志文件大小
    2.清空快速组网模式下-新组网的设备异常绑定的组号（getNodesWhitOutGroup接口无数据的问题）
    3.更新LK8720_MESH_GD_PA_V000047_20240320.bin资源

### V1.6.4
    1.优化日志输出
    2.新增清空日志接口
    
### V1.6.5
    1.修复批量组网过程中，因前一个设备配网过程中异常断开导致后一个设备未取消超时组网失败的问题。
