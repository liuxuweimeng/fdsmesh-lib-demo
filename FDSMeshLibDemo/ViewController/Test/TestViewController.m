//
//  TestViewController.m
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/7/11.
//

#import "TestViewController.h"
#import "TestFxViewController.h"

@interface TestViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation TestViewController

#pragma mark - Initialize
- (void)InitializeCode{
    if ([self.testModel isKindOfClass:[FDSMeshNodeModel class]]) {
        FDSMeshNodeModel *model = self.testModel;
        self.title = model.name;
    } else {
        FDSMeshGroupModel *model = self.testModel;
        self.title = model.name;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self InitializeCode];
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    BOOL isNode = [self.testModel isKindOfClass:[FDSMeshNodeModel class]];
    if (section == 0) {
        // V2接口
        return isNode ? 9 : 6;
    } else if (section == 1){
        // V3接口
        return isNode ? 8 : 7;
    } else {
        // V3新增接口
        return 3;
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"V2接口调用";
    } else if (section == 1){
        return @"V3接口调用";
    } else {
        return @"V3新增接口";
    }
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"修改灯光开关";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"修改灯光RGBW";
        } else if (indexPath.row == 2) {
            cell.textLabel.text = @"修改灯光CCT";
        } else if (indexPath.row == 3) {
            cell.textLabel.text = @"修改灯光HSI";
        } else if (indexPath.row == 4) {
            cell.textLabel.text = @"修改灯光色卡";
        } else if (indexPath.row == 5) {
            cell.textLabel.text = @"修改灯光特效";
        } else if (indexPath.row == 6) {
            cell.textLabel.text = @"修改设备风扇";
        } else if (indexPath.row == 7) {
            cell.textLabel.text = @"获取蓝牙固件版本";
        } else if (indexPath.row == 8) {
            cell.textLabel.text = @"获取电池电量信息";
        }
    } else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            cell.textLabel.text = @"修改灯光特效";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"修改灯光色卡";
        } else if (indexPath.row == 2) {
            cell.textLabel.text = @"修改灯光XY";
        } else if (indexPath.row == 3) {
            cell.textLabel.text = @"修改灯光RGBW";
        } else if (indexPath.row == 4) {
            cell.textLabel.text = @"修改灯光RGBWW";
        } else if (indexPath.row == 5) {
            cell.textLabel.text = @"修改灯光RGBACL";
        } else if (indexPath.row == 6) {
            cell.textLabel.text = @"修改亮度偏移";
        } else if (indexPath.row == 7) {
            cell.textLabel.text = @"获取MCU固件版本";
        }
    } else {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"修改灯光RGBW(扩展2)";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"修改灯光色卡(扩展2)";
        } else if (indexPath.row == 2) {
            cell.textLabel.text = @"修改灯光XY(扩展)";
        }
    }
    cell.detailTextLabel.text = @"";
    return cell;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UInt16 address = 0xFFFF;
    if ([self.testModel isKindOfClass:[FDSMeshNodeModel class]]) {
        FDSMeshNodeModel *model = self.testModel;
        address = model.address;
    } else {
        FDSMeshGroupModel *model = self.testModel;
        address = model.address;
    }
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            // 修改灯光开关
            [FDSCommandApi changeLightSwitch:address isSwitch:YES];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 1) {
            // 修改灯光RGBW
            [FDSCommandApi changeLightRGBW:address brightness:1 brightness_point:9 red:255 green:0 blue:0 white:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 2) {
            // 修改灯光CCT
            [FDSCommandApi changeLightCCT:address brightness:1 brightness_point:9 temperature:50 gm:0 circle:0 v2gm:-128];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 3) {
            // 修改灯光HSI
            [FDSCommandApi changeLightHSI:address brightness:0 brightness_point:9 hue:180 sat:50 mode:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 4) {
            // 修改灯光色卡
            [FDSCommandApi changeLightCard:address brightness:1 brightness_point:9 brand:0 number:0 sat:0 hue:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 5) {
            // 修改灯光特效
            [FDSCommandApi changeLightFX:address brightness:1 brightness_point:9 symbol:0 speed:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 6) {
            // 修改设备风扇
            [FDSCommandApi changeElectricFan:address mode:2 speed:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 7) {
            // 获取蓝牙固件版本
            [FDSCommandApi getBleVersion:address completion:^(NSString * _Nonnull version, BOOL isPA) {
                [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"蓝牙固件版本：%@, 是否为PA固件：%@",version, isPA?@"YES":@"NO"]];
            }];
        } else if (indexPath.row == 8) {
            // 获取电池电量信息
            [FDSCommandApi getBatteryPower:address completion:^(NSInteger state, NSInteger hour, NSInteger minute, NSInteger option, NSInteger power) {
                [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:
                                                      @"充电状态：%ld，使用时间小时部分：%ld, 使用时间分钟部分：%ld, 电量格式：%ld, 电量：%ld",
                                                      (long)state, (long)hour, (long)minute, (long)option, (long)power]];
            }];
        }
    } else if (indexPath.section == 1){
        if (indexPath.row == 0) {
            // 修改灯光特效
            TestFxViewController *viewController = [[TestFxViewController alloc] init];
            viewController.testModel = self.testModel;
            [self.navigationController pushViewController:viewController animated:YES];
        } else if (indexPath.row == 1){
            // 修改灯光色卡
            [FDSCommandApi changeLightCard_Ex:address brightness:50 brightness_point:9 brand:0 number:8 temperature:0 sat:0 hue:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 2) {
            // 修改灯光XY
            [FDSCommandApi changeLightXY:address brightness:50 brightness_point:9 X:1000 Y:2000];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 3) {
            // 修改灯光RGBW
            [FDSCommandApi changeLightRGBW_Ex:address brightness:50 brightness_point:9 type:0 red:255 green:0 blue:0 color1:255 color2:0 color3:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 4) {
            // 修改灯光RGBWW
            [FDSCommandApi changeLightRGBW_Ex:address brightness:50 brightness_point:9 type:1 red:255 green:0 blue:0 color1:255 color2:255 color3:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 5) {
            // 修改灯光RGBACL
            [FDSCommandApi changeLightRGBW_Ex:address brightness:50 brightness_point:9 type:2 red:255 green:0 blue:0 color1:255 color2:255 color3:255];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 6) {
            // 修改亮度偏移
            [FDSCommandApi changeBrightnessOffset:0xFFFF brightness:0 brightness_point:0];
            [SVProgressHUD showSuccessWithStatus:@"发送完成"];
        } else if (indexPath.row == 7){
            // 获取MCU固件版本
            [FDSCommandApi getMcuVersion:address completion:^(NSString * _Nonnull productVersion, NSString * _Nonnull mcuVersion) {
                [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"产品版本：%@，MCU方案版本：%@",productVersion, mcuVersion]];
            }];
        }
    } else {
        if (indexPath.row == 0) {
            // 修改灯光RGBW(扩展2)
            [FDSCommandApi changeLightRGBW_Ex2:address brightness:50 brightness_point:9 type:0 red:255 green:0 blue:0 color1:0 color2:0 color3:0];
        } else if (indexPath.row == 1) {
            // 修改灯光色卡(扩展2)
            [FDSCommandApi changeLightCard_Ex2:address brightness:50 brightness_point:9 brand:0 number:8 temperature:8 sat:8 hue:8 brandType:0 cardNumber:0];
        } else if (indexPath.row == 2) {
            // 修改灯光XY(扩展)
            [FDSCommandApi changeLightXY_Ex:address brightness:50 brightness_point:9 X:1000 Y:1000 colorGamut:4];
        }
    }
}

@end
