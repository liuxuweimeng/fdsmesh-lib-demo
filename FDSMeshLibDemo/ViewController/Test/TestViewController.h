//
//  TestViewController.h
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/7/11.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TestViewController : BaseViewController

@property (nonatomic) id testModel;

@end

NS_ASSUME_NONNULL_END
