//
//  TestFxViewController.m
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/7/26.
//

#import "TestFxViewController.h"

@interface TestFxViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableview;

@end

@implementation TestFxViewController

#pragma mark - Initialize
- (void)InitializeCode{
    if ([self.testModel isKindOfClass:[FDSMeshNodeModel class]]) {
        FDSMeshNodeModel *model = self.testModel;
        self.title = model.name;
    } else {
        FDSMeshGroupModel *model = self.testModel;
        self.title = model.name;
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self InitializeCode];
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 21;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 70;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    return @"V3特效接口调用";
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] ;
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    if (indexPath.row == 0) {
        cell.textLabel.text = @"闪光灯";
    } else if (indexPath.row == 1) {
        cell.textLabel.text = @"雷闪电";
    } else if (indexPath.row == 2) {
        cell.textLabel.text = @"多云";
    } else if (indexPath.row == 3) {
        cell.textLabel.text = @"坏灯泡";
    } else if (indexPath.row == 4) {
        cell.textLabel.text = @"电视机";
    } else if (indexPath.row == 5) {
        cell.textLabel.text = @"蜡烛";
    } else if (indexPath.row == 6) {
        cell.textLabel.text = @"火";
    } else if (indexPath.row == 7) {
        cell.textLabel.text = @"烟花";
    } else if (indexPath.row == 8) {
        cell.textLabel.text = @"爆炸";
    } else if (indexPath.row == 9) {
        cell.textLabel.text = @"焊接";
    } else if (indexPath.row == 10) {
        cell.textLabel.text = @"警车";
    } else if (indexPath.row == 11) {
        cell.textLabel.text = @"SOS";
    } else if (indexPath.row == 12) {
        cell.textLabel.text = @"彩光循环";
    } else if (indexPath.row == 13) {
        cell.textLabel.text = @"激光彩灯";
    } else if (indexPath.row == 14) {
        cell.textLabel.text = @"彩光渐入";
    } else if (indexPath.row == 15) {
        cell.textLabel.text = @"彩光流动";
    } else if (indexPath.row == 16) {
        cell.textLabel.text = @"彩光追逐";
    } else if (indexPath.row == 17) {
        cell.textLabel.text = @"音乐";
    } else if (indexPath.row == 18) {
        cell.textLabel.text = @"像素火";
    } else if (indexPath.row == 19) {
        cell.textLabel.text = @"像素蜡烛";
    } else if (indexPath.row == 20) {
        cell.textLabel.text = @"彩虹";
    }
    cell.detailTextLabel.text = @"";
    return cell;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UInt16 address = 0xFFFF;
    if ([self.testModel isKindOfClass:[FDSMeshNodeModel class]]) {
        FDSMeshNodeModel *model = self.testModel;
        address = model.address;
    } else {
        FDSMeshGroupModel *model = self.testModel;
        address = model.address;
    }
    if (indexPath.row == 0) {
        // 闪光灯
        [FDSCommandApi changeLightFX_Flash:address brightness:50 brightness_point:9 speed:1 trigger:1 mode:0 option:0 optionValue:50 gm:0];
    } else if (indexPath.row == 1) {
        // 雷闪电
        [FDSCommandApi changeLightFX_Lightning:address brightness:50 brightness_point:9 frequency:2 trigger:1 twinkling:0 temperature:50];
    } else if (indexPath.row == 2) {
        // 多云
        [FDSCommandApi changeLightFX_Cloudy:address brightness:50 brightness_point:0 speed:0 lightDark:10];
    } else if (indexPath.row == 3) {
        // 坏灯泡
        [FDSCommandApi changeLightFX_BrokenBulb:address brightness:50 brightness_point:9 speed:0 option:0 optionValue:50 gm:0];
    } else if (indexPath.row == 4) {
        // 电视机
        [FDSCommandApi changeLightFX_TV:address brightness:50 brightness_point:0 speed:1 option:0];
    } else if (indexPath.row == 5) {
        // 蜡烛
        [FDSCommandApi changeLightFX_Candle:address brightness:50 brightness_point:0 speed:1];
    } else if (indexPath.row == 6) {
        // 火
        [FDSCommandApi changeLightFX_Fire:address brightness:50 brightness_point:0 speed:1];
    } else if (indexPath.row == 7) {
        // 烟花
        [FDSCommandApi changeLightFX_Firework:address brightness:50 brightness_point:0 speed:1 ember:1];
    } else if (indexPath.row == 8) {
        // 爆炸
        [FDSCommandApi changeLightFX_Explode:address brightness:50 brightness_point:5 speed:1 ember:1 trigger:0 option:0 optionValue:50 gm:0];
    } else if (indexPath.row == 9) {
        // 焊接
        [FDSCommandApi changeLightFX_Welding:address brightness:50 brightness_point:9 speed:1 option:0 optionValue:50 gm:0];
    } else if (indexPath.row == 10) {
        // 警车
        [FDSCommandApi changeLightFX_PoliceCar:address brightness:50 brightness_point:9 mode:1 color:0];
    } else if (indexPath.row == 11) {
        // SOS
        [FDSCommandApi changeLightFX_SOS:address brightness:50 brightness_point:9 option:0 optionValue:50 gm:0];
    } else if (indexPath.row == 12) {
        // 彩光循环
        [FDSCommandApi changeLightFX_RGBCycle:address brightness:50 brightness_point:9 speed:1 sat:0];
    } else if (indexPath.row == 13) {
        // 激光彩灯
        [FDSCommandApi changeLightFX_Laser:address brightness:50 brightness_point:9 speed:1 sat:0];
    } else if (indexPath.row == 14) {
        // 彩光渐入
        [FDSCommandApi changeLightFX_RGBFadeIn:address brightness:50 brightness_point:9 speed:1 direction:0 colorLength:3 background:[[FDSColorBlockModel alloc] initWithOption:1 optionValue:360 sat:0] colorBlockList:@[
            [[FDSColorBlockModel alloc] initWithOption:0 optionValue:100 sat:0],
            [[FDSColorBlockModel alloc] initWithOption:1 optionValue:360 sat:0],
            [[FDSColorBlockModel alloc] initWithOption:2 optionValue:0xFF sat:0],
        ]];
    } else if (indexPath.row == 15) {
        // 彩光流动
        [FDSCommandApi changeLightFX_RGBFlow:address brightness:50 brightness_point:9 speed:1 direction:0 colorLength:3 colorBlockList:@[
            [[FDSColorBlockModel alloc] initWithOption:0 optionValue:100 sat:0],
            [[FDSColorBlockModel alloc] initWithOption:1 optionValue:360 sat:0],
            [[FDSColorBlockModel alloc] initWithOption:2 optionValue:0xFF sat:0],
        ]];
    } else if (indexPath.row == 16) {
        // 彩光追逐
        [FDSCommandApi changeLightFX_RGBChase:address brightness:50 brightness_point:9 speed:1 direction:0 mode:0 colorLength:3 colorBlockList:@[
            [[FDSColorBlockModel alloc] initWithOption:0 optionValue:100 sat:0],
            [[FDSColorBlockModel alloc] initWithOption:1 optionValue:360 sat:0],
            [[FDSColorBlockModel alloc] initWithOption:2 optionValue:0xFF sat:0],
        ]];
    } else if (indexPath.row == 17) {
        // 音乐
        [FDSCommandApi changeLightFX_Music:address brightness:50 brightness_point:9 mode:0];
    } else if (indexPath.row == 18) {
        // 像素火
        [FDSCommandApi changeLightFX_PixelFire:address brightness:50 brightness_point:9 hue:0 speed:1];
    } else if (indexPath.row == 19) {
        // 像素蜡烛
        [FDSCommandApi changeLightFX_PixelCandle:address brightness:50 brightness_point:9 hue:0 speed:1];
    } else if (indexPath.row == 20) {
        // 彩虹（注意背景色需要传亮度，色块不需要传亮度）
        [FDSCommandApi changeLightFX_Rainbow:address brightness:50 brightness_point:9 pixelCount:4 colorLength:3 speed:1 direction:0 background:
            [[FDSRainbowColorBlockModel alloc] initWithTemperature:27 pile:0 hue:0 sat:50 brightness:50]
            colorBlockList:@[
            [[FDSRainbowColorBlockModel alloc] initWithTemperature:32 pile:1 hue:180 sat:50],
            [[FDSRainbowColorBlockModel alloc] initWithTemperature:56 pile:2 hue:360 sat:50],
        ]];
    }
    [SVProgressHUD showSuccessWithStatus:@"发送完成"];
}

@end
