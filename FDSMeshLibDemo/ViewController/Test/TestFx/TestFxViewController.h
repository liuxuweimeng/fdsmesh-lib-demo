//
//  TestFxViewController.h
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/7/26.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface TestFxViewController : BaseViewController

@property (nonatomic) id testModel;

@end

NS_ASSUME_NONNULL_END
