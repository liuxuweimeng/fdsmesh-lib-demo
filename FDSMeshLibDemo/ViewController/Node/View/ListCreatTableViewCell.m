//
//  ListCreatTableViewCell.m
//  NELSON
//
//  Created by MACBOOKPRO on 2021/2/12.
//

#import "ListCreatTableViewCell.h"

@implementation ListCreatTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.creatButton.layer.cornerRadius = 22;
    self.creatButton.layer.masksToBounds = YES;
}

#pragma mark - UIButton
- (IBAction)creatButton:(UIButton *)sender {
    if (self.delegate&&[self.delegate respondsToSelector:@selector(getListCreatResponse)]) {
        [self.delegate getListCreatResponse];
    }
}

@end
