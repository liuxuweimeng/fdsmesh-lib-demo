//
//  NodeListTableViewCell.m
//  NELSON
//
//  Created by LEIPENG on 2021/2/18.
//

#import "NodeListTableViewCell.h"

@implementation NodeListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.cornerRadius = 10;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.04].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 8;
}

#pragma mark - UIButton
- (IBAction)clickEditBtn:(UIButton *)sender {
    if (self.delegate&&[self.delegate respondsToSelector:@selector(getNodeEventResponseEdit:)]) {
        [self.delegate getNodeEventResponseEdit:self.rowIndex];
    }
}
- (IBAction)clickSwitchBtn:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.delegate&&[self.delegate respondsToSelector:@selector(getNodeEventResponseSwitch:sw:)]) {
        [self.delegate getNodeEventResponseSwitch:self.rowIndex sw:sender.selected];
    }
}

@end
