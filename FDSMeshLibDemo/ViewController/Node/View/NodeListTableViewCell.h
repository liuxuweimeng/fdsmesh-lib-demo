//
//  NodeListTableViewCell.h
//  NELSON
//
//  Created by LEIPENG on 2021/2/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GetNodeEventResponseDelegate <NSObject>

- (void)getNodeEventResponseEdit:(NSInteger)rowIndex;
- (void)getNodeEventResponseSwitch:(NSInteger)rowIndex sw:(BOOL)isSw;

@end
@interface NodeListTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageview;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *switchButton;

@property (nonatomic, assign) NSInteger rowIndex;
@property (nonatomic, weak) id<GetNodeEventResponseDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
