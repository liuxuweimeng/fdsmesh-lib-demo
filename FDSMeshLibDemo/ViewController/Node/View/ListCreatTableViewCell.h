//
//  ListCreatTableViewCell.h
//  NELSON
//
//  Created by MACBOOKPRO on 2021/2/12.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol GetListCreatResponseDelegate <NSObject>

- (void)getListCreatResponse;

@end
@interface ListCreatTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *creatButton;
@property (weak, nonatomic) IBOutlet UILabel *creatLabel;
@property (nonatomic, weak) id<GetListCreatResponseDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
