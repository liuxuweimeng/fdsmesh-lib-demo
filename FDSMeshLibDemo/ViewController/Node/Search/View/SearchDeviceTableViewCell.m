//
//  SearchDeviceTableViewCell.m
//  NELSON
//
//  Created by LEIPENG on 2021/2/18.
//

#import "SearchDeviceTableViewCell.h"

@implementation SearchDeviceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bgView.layer.cornerRadius = 10;
    self.bgView.layer.shadowColor = [UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.04].CGColor;
    self.bgView.layer.shadowOffset = CGSizeMake(0,0);
    self.bgView.layer.shadowOpacity = 1;
    self.bgView.layer.shadowRadius = 8;
}

@end
