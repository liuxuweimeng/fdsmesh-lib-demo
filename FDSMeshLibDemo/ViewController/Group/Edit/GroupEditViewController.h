//
//  GroupEditViewController.h
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/6/10.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface GroupEditViewController : BaseViewController

@property (nonatomic, assign) UInt16 address;

@end

NS_ASSUME_NONNULL_END
