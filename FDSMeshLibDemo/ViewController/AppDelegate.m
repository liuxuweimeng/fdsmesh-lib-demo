//
//  AppDelegate.m
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/5/17.
//

#import "AppDelegate.h"
#import <Bugly/Bugly.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Bugly AppId
    [Bugly startWithAppId:@"92ec9b5b17"];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    return YES;
}

@end
