//
//  BaseViewController.m
//  GodoxLight
//
//  Created by LEIPENG on 2021/11/21.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

#pragma mark - Initialize
- (void)InitializeBaseCode{
    [self configLeftBarButtonItemWithImageName:@"list_03"];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self InitializeBaseCode];
}

- (void)configLeftBarButtonItemWithImageName:(NSString *)imageName{
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(leftBarButtonItemClick:)];
}
- (void)configRightBarButtonItemWithTitle:(NSString *)title{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemClick:)];
}
- (void)configRightBarButtonItemWithImageName:(NSString *)imageName{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemClick:)];
}
- (void)configRightBarButtonItemsWithImageName:(NSArray *)imageNameArray{
    NSMutableArray *btnArray = [NSMutableArray array];
    for (NSInteger i = 0; i < imageNameArray.count; i++) {
        UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:imageNameArray[i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(rightBarButtonItemsClick:)];
        right.tag = 100 + i;
        [btnArray addObject:right];
    }
    self.navigationItem.rightBarButtonItems = btnArray;
}
#pragma mark - UIBarButtonItem
- (void)leftBarButtonItemClick:(id)sender{
    if ([self.navigationController viewControllers].count == 1) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)rightBarButtonItemClick:(id)sender{
    
}
- (void)rightBarButtonItemsClick:(UIBarButtonItem *)item{
    
}

@end
