//
//  BaseViewController.h
//  GodoxLight
//
//  Created by LEIPENG on 2021/11/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

- (void)configRightBarButtonItemWithTitle:(NSString *)title;
- (void)configLeftBarButtonItemWithImageName:(NSString *)imageName;
- (void)configRightBarButtonItemWithImageName:(NSString *)imageName;
- (void)configRightBarButtonItemsWithImageName:(NSArray *)imageNameArray;

- (void)leftBarButtonItemClick:(id)sender;
- (void)rightBarButtonItemClick:(id)sender;
- (void)rightBarButtonItemsClick:(UIBarButtonItem *)item;

@end

NS_ASSUME_NONNULL_END
