//
//  MeshTool.h
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/5/31.
//

#import <Foundation/Foundation.h>
#import <FDSMeshLib/FDSMeshLib.h>
#import "MeshModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MeshTool : NSObject

+ (id)shareInstance;

// Studio列表
@property (nonatomic, strong) MeshStudioModel *studioModel;
@property (nonatomic, strong) MeshModel *meshModel;

// MESH列表
@property (nonatomic, strong) NSMutableArray<FDSMeshNodeModel *> *nodeList;
@property (nonatomic, strong) NSMutableArray<FDSMeshGroupModel *> *groupList;
@property (nonatomic, strong) NSMutableArray<FDSMeshNodeModel *> *nodeListWhitOutGroup;

// 刷新节点和组
- (void)refreshNodesAndGroups;

// 发送数据
- (void)sendMessageAddress:(UInt16)address sendData:(NSData *)sendData isResponse:(BOOL)isResponse completion:(void (^)(NSData *responseData))completion;

// 数据归档
- (MeshModel *)getMeshModel;
- (MeshStudioModel *)getMeshStudioModel:(NSString *)title;
- (void)codingMeshData:(MeshModel *)meshModel;
- (MeshModel *)unCodeingMeshData;

// 字符串和JSON转换
+ (NSDictionary *)jsonStringToDic:(NSString *)jsonString;
+ (id)jsonStringToObj:(NSString *)jsonString;

@end

NS_ASSUME_NONNULL_END
