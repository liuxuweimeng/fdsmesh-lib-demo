//
//  MeshTool.m
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/5/31.
//

#import "MeshTool.h"

#define SAVE_LOG   @"SAVE_LOG"
#define FDS_APP_ID @"185BD3FB2532A7CE6BF4B2C15B8C27F06E0554779140BF726A929128FD0514BE"

@implementation MeshTool

+ (id)shareInstance{
    static MeshTool *meshTool =  nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        meshTool = [[MeshTool alloc] init];
    });
    return meshTool;
}

- (id)init{
    if (self = [super init]) {
        [self Initialize];
    }
    return self;
}

#pragma mark - 初始化
- (void)Initialize{
    // 初始化SDK
    [FDSMeshApi startWithAppId:FDS_APP_ID];
    // 获取SDK版本
    NSString *version = [FDSMeshApi getVersion];
    // 配置日志
    [FDSMeshApi openSdkLog:YES];
    // 配置连接参数
    [FDSMeshApi configConnectParams:70.0 serviceTimeout:20.0 retryCount:2];
    // 初始化数组
    self.meshModel = [self getMeshModel];
    self.nodeList = [NSMutableArray array];
    self.groupList = [NSMutableArray array];
    self.nodeListWhitOutGroup = [NSMutableArray array];
    BOOL isClearLog = NO;
    if (self.meshModel.sdkVersion && self.meshModel.sdkVersion.length) {
        if ([version compare:self.meshModel.sdkVersion] == NSOrderedDescending) {
            isClearLog = YES;
        }
    } else {
        isClearLog = YES;
    }
    if (isClearLog) {
        [FDSMeshApi clearSdkLog];
    }
    self.meshModel.sdkVersion = version;
    [self codingMeshData:self.meshModel];
}

#pragma mark - 刷新节点和组
- (void)refreshNodesAndGroups{
    [FDSMeshApi refreshNodesAndGroups];
    [self.nodeList setArray:[FDSMeshApi getNodes]];
    [self.groupList setArray:[FDSMeshApi getGroups]];
    [self.nodeListWhitOutGroup setArray:[FDSMeshApi getNodesWhitOutGroup]];
}

#pragma mark - 发送数据
- (void)sendMessageAddress:(UInt16)address sendData:(NSData *)sendData isResponse:(BOOL)isResponse completion:(void (^)(NSData *responseData))completion{
    [FDSMeshApi sendMessageAddress:address sendData:sendData opcode:0xf0 vendorId:0x0211 responseMax:isResponse?1:0 responseOpcode:isResponse?0xf1:0xf0 retryCount:0 completion:completion];
}

#pragma mark - 数据归档
- (MeshModel *)getMeshModel{
    MeshModel *model = [self unCodeingMeshData];
    if (nil == model) {
        MeshStudioModel *studioModel = [[MeshStudioModel alloc] init];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm:ss";
        studioModel.title = @"studio-1";
        studioModel.subTitle = [dateFormatter stringFromDate:[NSDate date]];
        studioModel.meshJson = [FDSMeshApi getInitMeshJson];
        MeshModel *creatModel = [[MeshModel alloc] init];
        creatModel.isFastProvision = NO;
        creatModel.studioList = @[studioModel];
        [self codingMeshData:creatModel];
        return creatModel;
    } else {
        return model;
    }
}
- (MeshStudioModel *)getMeshStudioModel:(NSString *)title{
    MeshStudioModel *studioModel = [[MeshStudioModel alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat=@"yyyy-MM-dd HH:mm:ss";
    studioModel.title = title;
    studioModel.subTitle = [dateFormatter stringFromDate:[NSDate date]];
    studioModel.meshJson = [FDSMeshApi getInitMeshJson];
    return studioModel;
}
- (void)codingMeshData:(MeshModel *)meshModel{
    NSString * dataPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject];
    NSString * dataPathName = [dataPath stringByAppendingString:@"/FDS_MeshData.data"];
    [NSKeyedArchiver archiveRootObject:meshModel toFile:dataPathName];
}
- (MeshModel *)unCodeingMeshData{
    NSString * dataPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject];
    NSString * dataPathName = [dataPath stringByAppendingString:@"/FDS_MeshData.data"];
    MeshModel *meshModel = [NSKeyedUnarchiver unarchiveObjectWithFile:dataPathName];
    return meshModel;
}

#pragma mark - Json字符串转字典
+ (NSDictionary *)jsonStringToDic:(NSString *)jsonString{
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err){
        return nil;
    }
    return dic;
}
+ (id)jsonStringToObj:(NSString *)jsonString{
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    id obj = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err){
        return nil;
    }
    return obj;
}

@end
