//
//  MeshModel.h
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/6/9.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MeshStudioModel : NSObject

// 标题
@property (nonatomic, strong) NSString *title;

// 副标题
@property (nonatomic, strong) NSString *subTitle;

// 组网数据
@property (nonatomic, strong) NSString *meshJson;

- (id)initWithModel:(MeshStudioModel *)tmpModel;

@end

@interface MeshModel : NSObject

@property (nonatomic, assign) BOOL isFastProvision;

@property (nonatomic, strong) NSString *sdkVersion;

// Studio列表
@property (nonatomic, copy) NSArray<MeshStudioModel *> * studioList;

@end

NS_ASSUME_NONNULL_END
