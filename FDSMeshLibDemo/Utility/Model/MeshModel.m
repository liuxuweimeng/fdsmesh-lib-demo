//
//  MeshModel.m
//  FDSMeshLibDemo
//
//  Created by LEIPENG on 2022/6/9.
//

#import "MeshModel.h"

@implementation MeshStudioModel

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.title forKey:@"title"];
    [aCoder encodeObject:self.subTitle forKey:@"subTitle"];
    [aCoder encodeObject:self.meshJson forKey:@"meshJson"];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.title = [aDecoder decodeObjectForKey:@"title"];
        self.subTitle = [aDecoder decodeObjectForKey:@"subTitle"];
        self.meshJson = [aDecoder decodeObjectForKey:@"meshJson"];
    }
    return self;
}

- (id)initWithModel:(MeshStudioModel *)tmpModel{
    // 深拷贝
    if (self = [super init]) {
        self.title = tmpModel.title;
        self.subTitle = tmpModel.subTitle;
        self.meshJson = tmpModel.meshJson;
    }
    return self;
}

@end

@implementation MeshModel

-(void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeBool:self.isFastProvision forKey:@"isFastProvision"];
    [aCoder encodeObject:self.sdkVersion forKey:@"sdkVersion"];
    [aCoder encodeObject:self.studioList forKey:@"studioList"];
}
-(id)initWithCoder:(NSCoder *)aDecoder{
    if (self = [super init]) {
        self.isFastProvision = [aDecoder decodeBoolForKey:@"isFastProvision"];
        self.sdkVersion = [aDecoder decodeObjectForKey:@"sdkVersion"];
        self.studioList = [aDecoder decodeObjectForKey:@"studioList"];
    }
    return self;
}
+(NSDictionary *)objectClassInArray{
    return @{
        @"studioList" : [MeshStudioModel class],
    };
}

@end
